const http = require("http");
const port = 4000;

const server = http.createServer((request, response) => {
  // HTTP Routing Methods : GET, POST, PUT, DELETE
  if (request.url == "/items" && request.method == "GET") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Data retrievd from database!");
  } else if (request.url == "/items" && request.method == "POST") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Data sent to database!");
  } else {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.end("Not Found");
  }
});

server.listen(port);

console.log(`Server running at localhost:${port}`);
