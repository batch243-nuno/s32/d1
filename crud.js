const http = require('http');
const port = 4000;

let directory = [
  {
    name: 'Brandon',
    email: 'brandon@email.com',
  },
  {
    name: 'Jobert',
    email: 'brandon@email.com',
  },
];

const server = http.createServer((request, response) => {
  //Route for returning all items upon receiving a GET request
  if (request.url == '/users' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    // converted to string
    response.write(JSON.stringify(directory));
    response.end();
  }

  //  add user to database
  if (request.url == '/users' && request.method == 'POST') {
    // declare and initialize a "requestBody" variable to an empty string
    let requestBody = '';
    // data stream
    request.on('data', (data) => {
      requestBody += data;
    });
    // reposnse and step - only runs after the request
    request.on('end', () => {
      console.log(typeof requestBody);
      requestBody = JSON.parse(requestBody);
      //   created new object
      let newUser = {
        name: requestBody.name,
        email: requestBody.email,
      };
      //   add new user to database
      directory.push(newUser);
      console.log(directory);

      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.write(JSON.stringify(newUser));
      response.end();
    });
  }
});
server.listen(port);

console.log(`Server running at localhost:${port}`);
